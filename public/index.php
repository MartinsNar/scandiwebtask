<?php 

use App\Libraries\Core;

require_once '../app/bootstrap.php';

/**
 * main calss for starting the app
 * Core Class
 */
$init = new Core();
