const addForm = document.getElementById("addForm");
addForm.addEventListener("submit", function (e) {
e.preventDefault();

const xhr = new XMLHttpRequest();
const formData = new FormData(addForm);

xhr.open("post", addPath, true);
xhr.onload = function() {
        
        var data = JSON.parse(this.response);

        if (anyErrors(data)) {
            var response = document.getElementById("response");
            response.textContent = '';
            for (const item in data) {
                response.innerHTML += '<li>' + data[item] + '</li>';
              }
        } else {
            location.replace(data);
        }
}
xhr.send(formData);
});