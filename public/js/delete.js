const deleteForm = document.getElementById("deleteForm");
deleteForm.addEventListener("submit", function (e) {
e.preventDefault();

const xhr = new XMLHttpRequest();
const formData = new FormData(deleteForm);

xhr.open("post", deletePath, true);
xhr.send(formData);
xhr.onload = function() {
        var response = document.getElementById("response");
        response.textContent = this.response;
}
document.querySelectorAll('input[type=checkbox]:checked').forEach(func);
function func(item) {
    item.parentNode.parentNode.remove();
}
});