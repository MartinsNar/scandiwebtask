<?php

namespace App\Controllers\Interfaces;

interface Types
{
   /*
   * Getting specific data about the products
   * @param data to complement
   * @return complemented data about the product
   */
   public function getProductData($data);
}
