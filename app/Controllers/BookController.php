<?php

namespace App\Controllers;

use App\Controllers\Interfaces\Types;

class BookController implements Types
{
    public function getProductData($data)
    {
        if (empty($_POST["weight"])) {
            $data['errors']['attrValue_err'] = 'Please specify the weight in KG';
        } else {
            $data['attrValue'] = check_input($_POST['weight']);
        }
        return $data;
    }
}
