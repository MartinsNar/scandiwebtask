<?php

namespace App\Controllers;

use App\Controllers\Interfaces\Types;

class DvdController implements Types
{
    public function getProductData($data)
    {
        if (empty($_POST["size"])) {
        $data['errors']['attrValue_err'] = 'Please specify the size in MB';
        } else {
        $data['attrValue'] = check_input($_POST['size']);
        }
        return $data;
    }
}
