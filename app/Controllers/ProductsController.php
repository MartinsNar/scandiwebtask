<?php

namespace App\Controllers;

use App\Libraries\Controller;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->productModel = $this->model('Product');
    }

    /**
 * Default method
 * Loads the page with the products list
 */
    public function index()
    {
        $data = [
            'products' => $this->productModel->getProducts(),
        ];
        $this->view('products/index', $data);
    }

/**
 * Validate the incoming data
 * Add data to the Database
 */
    public function add()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            $data = [
                'SKU' => check_input($_POST['SKU']),
                'name' => check_input($_POST['name']),
                'price' => check_input($_POST['price']),
                'types' => check_input($_POST["types"]),
                'attrValue' => '',
                'errors' => []
            ];
            
            if ($this->productModel->SKUcheck($data['SKU'])) {
                $data['errors'][] = 'The product with this SKU already exists';
            }

            $data = dataValidation($data);

            //Requiring specific product class
            if (file_exists('../app/Controllers/' . ucwords($data['types']).'Controller.php')) {
                //Validate the selected type of product and correspoding fields
                $typesSelect = productController($data['types']);
                $data = $typesSelect->getProductData($data);
            } else {
                $data['errors'][] = 'Please select a product type';
            }
            
            if (empty($data['errors'])) {
                $this->productModel->addProduct($data);
                redirect('products');
            } else {
                //Load the view with errors
                echo json_encode($data['errors']);
            }

        } else {
        $data = [
            'SKU' => '',
            'name' => '',
            'price' => '',
            'types' => '',
            'attrValue' => '',
        ];
        
        $this->view('products/add', $data);
        }
    }
    
    /**
     * Deleting the products from DB
     */
    public function delete()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['deletebox'])) {
                $this->productModel->deleteProduct($_POST['deletebox']);
            } else {
                echo 'No product selected';
            }
        } else {
        header("Location:" . URLROOT . '/products');
        }
    }
 }
 