<?php

namespace App\Controllers;

use App\Controllers\Interfaces\Types;

class FurnitureController implements Types
{
    public function getProductData($data)
    {
            if (empty($_POST["height"]) || empty($_POST["width"]) || empty($_POST["length"])) {
                $data['errors']['attrValue_err'] = 'Please specify the correct furniture size';
            } else {
                $height = check_input($_POST['height']);
                $width = check_input($_POST['width']);
                $length = check_input($_POST['length']);
                $data['attrValue'] = $height.'x'.$width.'x'.$length;
            }
            return $data;
    }
}
