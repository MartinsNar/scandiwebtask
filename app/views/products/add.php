<?php require APPROOT . '/views/inc/header.php'; ?>
		<form action="<?= URLROOT; ?>/products/add" method="post" id="addForm">
      <div class="d-flex  p-3 px-md-4 mb-3 justify-content-between">
        <h3 class="my-0 mr-md-auto font-weight-normal">Product Add</h3>
        <div class="my-2 my-md-0 mr-md-3">
        <button type="submit" name="submit" class="btn btn-success">Save</button>
        <a href="<?= URLROOT; ?>/products" class="btn btn-danger">Cancel</a>
        </div>
      </div>
          <hr>
      <p>* required field</p>
      <div >
        <ul id="response" class="text-danger"></ul>
      </div>
      <div class="inputs">
              <label for="SKU">SKU: *</label>
              <input type="text" name="SKU" id="SKU" placeholder="Product SKU" class="form-control" value="<?= $data['SKU']; ?>">
              <label for="name">Name: *</label>
              <input type="text" name="name" id="name" placeholder="Product name" class="form-control" value="<?= $data['name']; ?>">
              <label for="price">Price: *</label>
              <input type="text" name="price" id="price" placeholder="Product price" class="form-control" value="<?= $data['price']; ?>">
              <br>
        Type of the product: 
        <select id="types" name="types" onchange="AdditAtt()">
            <option value="choose">Choose a type</option>
            <option value="dvd"
               >DVD-Disc</option>
            <option value="book"
                >Book</option>
            <option value="furniture" 
                >Furniture</option>
        </select>
        
        <br><br>
          <div id="dvd" class="adAtr">
                <label for="size">Size: *</label>
                <input type="text" name="size" id="size" placeholder="Product size" class="form-control" value="<?= $data['attrValue']; ?>">
              <p>Please indicate the size of the DVD-Disc in <strong>MB</strong></p>
          </div>
          <div id="book" class="adAtr">
                <label for="weight">Weight: *</label>
                <input type="text" name="weight" id="weight" placeholder="Product weight" class="form-control " value="<?= $data['attrValue']; ?>">
              <p>Please indicate the weight of the book in <strong>Kg</strong></p>
          </div>
          <div id="furniture" class="adAtr">
                  <label for="height">Height: *</label>
                <input type="text" name="height" id="height" placeholder="Product height" class="form-control">
                 <label for="width">Width: *</label>
                <input type="text" name="width" id="width" placeholder="Product width" class="form-control">
                  <label for="length">Length: *</label>
                <input type="text" name="length" id="length" placeholder="Product length" class="form-control">
              <p>Please indicate height, width and lenght of the furniture in <strong>cm</strong></p>
          </div>
          <div id="productAdditionalAttribute"></div>
      </div>
    </form>
<?php require APPROOT . '/views/inc/footer.php'; ?>