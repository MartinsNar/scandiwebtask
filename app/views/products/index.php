<?php require APPROOT . '/views/inc/header.php'; ?>
 <form action="<?= URLROOT; ?>/products/delete" method="post" id="deleteForm">
     <div class="d-flex  p-3 px-md-4 mb-3 justify-content-between">
        <h3 class="my-0 mr-md-auto font-weight-normal">Product list</h3>
        <div class="my-2 my-md-0 mr-md-3">
            <select id="listOptions" name="listOptions">
                  <option value="delete">Delete Mass Action</option>
            </select>
            <button type="submit" name="submit" class="btn btn-success">Apply</button>
            <p id="response" class="text-danger"></p>
        </div>
    </div>
    <hr>
    <div class="d-flex flex-wrap">
      <?php foreach ($data['products'] as $product) : 	?>
          <div class="card mb-4 shadow-sm">
            <div class="card-header d-flex justify-content-between">
              <h4 class="my-0 font-weight-normal"><?= $product['SKU']; ?></h4>
              <input type="checkbox" class="checkbox" name="deletebox[]" value="<?= $product['id']; ?>">
            </div>
            <div class="card-body">
              <h2 class="card-title text-center"><?= $product['name']; ?></h2>
                <p class="mt-3 mb-3 text-center"><strong>Price:</strong> <?= $product['price']; ?> $</p>
                <p class="mt-2 mb-3 text-center"><strong><?= $product['attribute'] ?>:</strong> <?= $product['attribute_value'] . $product['units']; ?></p>
            </div>
          </div>
      <?php endforeach; ?>
    </div>
    
 </form>
<?php require APPROOT . '/views/inc/footer.php'; ?>