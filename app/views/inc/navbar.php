<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-3">
  <div class="container">
      <a class="navbar-brand" href="<?= URLROOT; ?>/products"><?= SITENAME; ?></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?= URLROOT; ?>/products">List of products</a>
          </li>
          <li class="nav-item">
            <a href="<?= URLROOT; ?>/products/add" class="nav-link">
              <i class="fa fa-pencil"></i> Add new product
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>