<?php

namespace App\Libraries;

use App\Models\Product;

    /*
* Base Controller
* Loads the models and views
*/
class Controller
{
    /**
     * Load the model
     * @param model to load
     * @return instantiated model
     */
    public function model($model)
    {
        $model = 'App\Models\\'.$model;
        // Instatiate model
        return new $model();
    }

    /**
     * Loads the view
     * @param view path
     * @param data to display in the view
     */
    public function view($view, $data = [])
    {
        // Check for view file
        if (file_exists('../app/views/'. $view. '.php')) {
            //Require the view file
            require_once '../app/views/'. $view. '.php';
        } else {
            // View does not exist
            die('View does not exist');
        }
    }
}
