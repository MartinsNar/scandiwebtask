<?php

namespace App\Libraries;

use App\Controllers\ProductController;

/*
* App Core Class
* Creates URL & loads core controller
* URL FORMAT - /controller/method/params
*/
class Core
{
    protected $currentControllers = 'Products';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
        $url = $this->getUrl();

        // Look in controllers for first value
        if (isset($_GET['url'])) {
            if (file_exists('../app/controllers/' . ucwords($url[0]).'Controller.php')) {
                //If exists, set as controller
                $this->currentControllers = ucwords($url[0]);
                unset($url[0]);
            }
        }
        
        //Instantiate controller class
        $this->currentControllers = productController($this->currentControllers);

        //Check for second part of url
        if (isset($url[1])) {
            //Check to see if method exists in controller
            if (method_exists($this->currentControllers, $url[1])) {
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }

        //Get params
        $this->params = $url ? array_values($url) : [];

        // Call a class method with params
        call_user_func_array([$this->currentControllers, $this->currentMethod], $this->params);

    }

    /**
     * Sanitizes the URL
     * @return URL string
     */
    public function getUrl()
    {
        if (isset($_GET['url'])) {
            //Clean and filter incoming url
            $url = rtrim($_GET['url'], '/');
            $url = filter_var($url, FILTER_SANITIZE_URL);
            $url = explode('/', $url);
            return $url;
        }
    }
}
