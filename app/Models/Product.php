<?php

namespace App\Models;

use App\Libraries\Database;

class Product 
{
    private $db;

    public function __construct()
    {
        $this->db = new Database;
    }

    /**
     * Gets all the books from db
     * @return array with products
     */
    public function getProducts()
    {
        $this->db->query("SELECT p.*, pt.name AS type_name, pt.attribute, pt.units FROM `products` AS p
        LEFT JOIN `product_types` AS pt ON p.type_id = pt.id
        ");

        return $this->db->resultSet();
    }

    /**
    * Inserts data into DB
    * @param array with data ti insert
    * @return bool
    */
    public function addProduct($data)
    {
        $this->db->query('INSERT INTO products (SKU, name, price, attribute_value, type_id) VALUES (:SKU, :name, :price, :attrValue, 
        (SELECT id FROM `product_types` WHERE name = :type))');
        // Bind values
        $this->db->bind(':SKU', $data['SKU']);
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':price', $data['price']);
        $this->db->bind(':attrValue', $data['attrValue']);
        $this->db->bind(':type', $data['types']);

        // Execute
        if ($this->db->execute()) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Gets all the dvd from db
     * @param array products selected to delete
     */
    public function deleteProduct($checked)
    {
            foreach ($checked as $id) {
                $id = check_input($id);
                $this->db->query('DELETE FROM products WHERE id = :id');
                // Bind values
                $this->db->bind(':id', $id);
                // Execute
                $this->db->execute();
            }	
    }

    /**
     * Checks wether entered SKU already exists in the db
     * @param string to check
     * @return bool result
     */
    public function SKUcheck($SKU)
    {
            $this->db->query('SELECT * FROM products WHERE SKU = :SKU');
            $this->db->bind(':SKU', $SKU);
            $this->db->execute();

            if ($this->db->rowCount()) {
                return true;
            }
            return false;
    }
}
