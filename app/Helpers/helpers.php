<?php 

/**
 * Redirect to another page
 * @param page to redirect
 * @return full url to redirect
 */
function redirect($page)
{
    echo json_encode(URLROOT . '/' . $page);
}

/**
 * Validating the input from POST array
 * @param data to validate
 * @return validated data
 */
function check_input($data) 
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}

/**
 * Validates the input data
 * @param data to validate
 * @return validated data
 */
function dataValidation($data)
{
    //Validate SKU
    if (empty($data['SKU'])) {
        $data['errors'][] = 'Please enter the SKU';
    }

    //Validate Name
    if (empty($data['name'])) {
        $data['errors'][] = 'Please enter the product name';
    }

    //Validate price
    if (empty($data['price'])) {
        $data['errors'][] = 'Please enter the product price';
    } elseif(!is_numeric($data['price'])) {
        $data['errors'][] = 'The product price should be have a numeric value';
    }
    return $data;

}

/**
 * Returns the specific controller
 * @param product name
 * @return new product object
 */
function productController($product)
{
    $product = "App\Controllers\\" . ucwords($product) . "Controller";

    return new $product;
}
