<?php

    //Load the config file
    require_once 'Config/config.php';
    //Load the helper file
    require_once 'Helpers/helpers.php';

    //Autoload libraries
    require_once '../vendor/autoload.php';
